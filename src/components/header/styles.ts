import styled from "styled-components";

export const Container = styled.header`
  min-height: 5rem;
  background-color: #333;
`;

export const Content = styled.div`
  max-width: 1120px;
  min-height: 5rem;
  margin: 0 auto;
  padding: 0 2rem;
  display: flex;
  align-items: center;

  h5 {
    font-size: 1.75rem;
    color: white;
  }

  nav {
    margin-left: 5rem;
    min-height: 5rem;

    a {
      display: inline-block;
      text-decoration: none;
      position: relative;
      padding: 0 0.5rem;
      height: 100%;
      line-height: 5rem;
      color: #d0d0d0;
      transition: color 0.2s;

      & + a {
        margin-left: 2rem;
      }

      &:hover {
        color: #79ab7f;
      }
    }
  }
`;
