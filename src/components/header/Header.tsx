import React from "react";
import { Link } from "react-router-dom";

import { Container, Content } from "./styles";

export const Header = () => {
  return (
    <Container>
      <Content>
        <h5>Logo</h5>

        <nav>
          <Link to="/">Home</Link>
          <Link to="/contact">Contact Us</Link>
        </nav>
      </Content>
    </Container>
  );
};
