import React from "react";
import { Switch, Route } from "react-router-dom";

import { Contact, Dashboard } from "../pages";

export const Routes: React.FC = () => {
  return (
    <Switch>
      <Route path="/" exact component={Dashboard} />
      <Route path="/contact" component={Contact} />
    </Switch>
  );
};
