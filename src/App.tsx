import React from "react";
import { BrowserRouter } from "react-router-dom";

import { Header } from "./components/header";
import { Routes } from "./routes";

import { GlobalStyle } from "./styles/global";

export const App: React.FC = () => {
  return (
    <>
      <GlobalStyle />

      <BrowserRouter>
        <Header />
        <Routes />
      </BrowserRouter>
    </>
  );
};
