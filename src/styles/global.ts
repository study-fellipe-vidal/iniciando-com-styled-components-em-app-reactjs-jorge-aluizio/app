import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;

        font-family: sans-serif;
    }

    #root {
        height: 100%;
        max-width: 1200px;
        margin: 0 auto;
    }
`;
